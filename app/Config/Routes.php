<?php namespace Config;

// Create a new instance of our RouteCollection class.
$routes = Services::routes();

// Load the system's routing file first, so that the app and ENVIRONMENT
// can override as needed.
if (file_exists(SYSTEMPATH . 'Config/Routes.php'))
{
	require SYSTEMPATH . 'Config/Routes.php';
}

/**
 * --------------------------------------------------------------------
 * Router Setup
 * --------------------------------------------------------------------
 */
$routes->setDefaultNamespace('App\Controllers');
$routes->setDefaultController('Patient');
$routes->setDefaultMethod('index');
$routes->setTranslateURIDashes(false);
$routes->set404Override();
$routes->setAutoRoute(true);

/**
 * --------------------------------------------------------------------
 * Route Definitions
 * --------------------------------------------------------------------
 */

// We get a performance increase by specifying the default
// route since we don't have to scan directories.
$routes->get('/', 'Patient::index', ['filter' => 'auth']);
$routes->get('pdf/(:num)', 'Patient::pdf/$1', ['filter' => 'auth']);
$routes->match(['get', 'post'], 'create', 'Patient::create', ['filter' => 'auth']);
$routes->match(['get', 'post'], 'view/(:num)', 'Patient::view/$1', ['filter' => 'auth']);
$routes->match(['get', 'post'], 'edit/(:num)', 'Patient::edit/$1', ['filter' => 'auth']);
$routes->match(['get', 'post'], 'delete/(:num)', 'Patient::delete/$1', ['filter' => 'auth']);
$routes->match(['get', 'post'], 'create_medhistory', 'Patient::create_medhistory', ['filter' => 'auth']);
$routes->get('appointment', 'Appointment::index', ['filter' => 'auth']);
$routes->match(['get', 'post'], 'appointment/create', 'Appointment::create', ['filter' => 'auth']);
$routes->match(['get', 'post'], 'appointment/edit', 'Appointment::edit', ['filter' => 'auth']);
$routes->match(['get', 'post'], 'appointment/delete/(:num)', 'Appointment::delete/$1', ['filter' => 'auth']);
$routes->match(['get', 'post'], 'getDistrict/(:num)', 'Patient::getDistrict/$1', ['filter' => 'auth']);
$routes->match(['get', 'post'], 'getSubdistrict/(:num)', 'Patient::getSubdistrict/$1', ['filter' => 'auth']);
$routes->match(['get', 'post'], 'search_medhistory/', 'Patient::search_medhistory/', ['filter' => 'auth']);

$routes->get('logout', 'User::logout');
$routes->match(['get', 'post'], 'login', 'User::index', ['filter' => 'noauth']);
$routes->match(['get', 'post'], 'register', 'User::register', ['filter' => 'noauth']);
$routes->match(['get', 'post'], 'profile', 'User::profile', ['filter' => 'auth']);
$routes->get('approve', 'User::approve', ['filter' => 'admin']);
$routes->get('approve/(:num)/(:num)', 'User::approve/$1/$2', ['filter' => 'admin']);
$routes->match(['get', 'post'], 'deleteUser/(:num)', 'User::deleteUser/$1', ['filter' => 'admin']);
$routes->match(['get', 'post'], 'manage', 'User::mange', ['filter' => 'admin']);

/**
 * --------------------------------------------------------------------
 * Additional Routing
 * --------------------------------------------------------------------
 *
 * There will often be times that you need additional routing and you
 * need it to be able to override any defaults in this file. Environment
 * based routes is one such time. require() additional route files here
 * to make that happen.
 *
 * You will have access to the $routes object within that file without
 * needing to reload it.
 */
if (file_exists(APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php'))
{
	require APPPATH . 'Config/' . ENVIRONMENT . '/Routes.php';
}
