<?php

namespace App\Controllers;

class Appointment extends BaseController {
    
    public function index() {
        
        $data = [
            'appointment' => $this->appointment->getAppointmentJoinPatient(),
            'title' => 'ตารางนัดหมายผู้ป่วย'
        ];
        if (!empty($data['appointment']) && is_array($data ['appointment'])) {
            foreach ($data ['appointment'] as $key=>$item){
                $appointment = $this->appointment->getAppointment();
                $data['appointment'][$key]['id'] = $appointment[$key]['id'];
            }
        }

        echo view('templates/header', $data);
        echo view('templates/navbar');
        echo view('appointment/index', $data);
        echo view('templates/footer', $data);
    }
    
    public function create() {

        if ($this->request->getMethod() === 'post'){
            $patient = $this->patient->find($this->request->getPost('patient_id'));
            if (isset($patient)){
                $this->appointment->save([
                    'date' => $this->request->getPost('date'). ' ' .$this->request->getPost('hour'). ':' .$this->request->getPost('minute').':00',
                    'description' =>trim(preg_replace("/\s\s+/","</br>", $this->request->getPost('description'))),
                    'patient_id' => $this->request->getPost('patient_id')
                ]);

                 session()->setFlashdata('success-appointment', 'เพิ่มการนัดหมายผู้ป่วยสำเร็จแล้ว');
                 return redirect()->to('/view/' . $this->request->getPost('patient_id'));
            }else{
                session()->setFlashdata('no-data', 'ไม่พบข้อมูล ข้อมูลนี้อาจถูกลบไปแล้ว');
                return redirect()->to('/');
            }
        }
    }
    
    public function edit() {

        if ($this->request->getMethod() === 'post'){
            $appointment = $this->appointment->find($this->request->getPost('id'));
            if (isset($appointment)){
                $this->appointment->save([
                    'id' => $this->request->getPost('id'),
                    'date' => $this->request->getPost('date'). ' ' .$this->request->getPost('hour'). ':' .$this->request->getPost('minute').':00',
                    'description' =>trim(preg_replace("/\s\s+/","</br>", $this->request->getPost('description'))),
                ]);

                 session()->setFlashdata('success-appointment-edit', 'แก้ไขการนัดหมายผู้ป่วยสำเร็จแล้ว');
                 return redirect()->to('/appointment/');
            }else{
                session()->setFlashdata('appoint-no-data', 'ไม่พบข้อมูล ข้อมูลนี้อาจถูกลบไปแล้ว');
                return redirect()->to('/appointment/');
            }
        }
    }
    
    public function delete($id) {
        if(!empty($id)){
            $this->appointment->delete($id);
        }
        return redirect()->to('/appointment');
    }
}
