<?php
namespace App\Controllers;

/**
 * Class BaseController
 *
 * BaseController provides a convenient place for loading components
 * and performing functions that are needed by all your controllers.
 * Extend this class in any new controllers:
 *     class Home extends BaseController
 *
 * For security be sure to declare any new methods as protected or private.
 *
 * @package CodeIgniter
 */

use App\Models\PatientModel;
use App\Models\MedHistoryModel;
use App\Models\ProvinceModel;
use App\Models\DistrictModel;
use App\Models\UserModel;
use App\Models\AppointmentModel;
use App\Libraries\Address;
use App\Libraries\PDF;
use CodeIgniter\Session\Session;
use CodeIgniter\Controller;

helper('filesystem');
helper('url');
helper(['form']);


class BaseController extends Controller
{

    /**
     * An array of helpers to be loaded automatically upon
     * class instantiation. These helpers will be available
     * to all other controllers that extend BaseController.
     *
     * @var array
     */
    protected $helpers = [];
    protected $patient, $province, $district, $address;


    /**
     * Constructor.
     */
    public function initController(\CodeIgniter\HTTP\RequestInterface $request, \CodeIgniter\HTTP\ResponseInterface $response, \Psr\Log\LoggerInterface $logger)
    {
        // Do Not Edit This Line
        parent::initController($request, $response, $logger);
        
        $this->patient = new PatientModel();
        $this->medhistory = new MedHistoryModel();
        $this->province = new ProvinceModel();
        $this->district = new DistrictModel();
        $this->user = new UserModel();
        $this->appointment = new AppointmentModel();
        $this->address = new Address();
        $this->pdf = new PDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, 'UTF-8', false);
        
        date_default_timezone_set("Asia/Bangkok");
//        $this->session = new Session();
    
    }

}
