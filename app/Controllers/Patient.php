<?php

namespace App\Controllers;

use CodeIgniter\Controller;
helper(['form', 'url']);

class Patient extends BaseController{
    
    public function index() {
        
        $data = [
            'patient' => $this->patient->getPatient(),
            'title' => 'รายชื่อผู้ป่วย'
        ];
        echo view('templates/header', $data);
        echo view('templates/navbar');
        echo view('patient/patient', $data);
        echo view('templates/footer', $data);
    }
    
    public function view($id) {
        
        $data['patient'] = $this->patient->find($id);
        if (isset($data['patient'])){
            $data ['medhistory'] = $this->medhistory->getMedhistoryFromPatientId($id);
            $data['address'] = $this->address->getAddress($data['patient']['province'], $data['patient']['district'], $data['patient']['subdistrict']);
            $data['title'] = 'ข้อมูลผู้ป่วย';

            echo view('templates/header', $data);
            echo view('templates/navbar');
            echo view('patient/detail', $data);
            echo view('templates/footer', $data);
        }else{
            session()->setFlashdata('no-data', 'ไม่พบข้อมูล ข้อมูลนี้อาจถูกลบไปแล้ว');
            return redirect()->to('/');
        }
    }
    
    public function search_medhistory(){
        $id = $this->request->getPost('id');
        $from_date = $this->request->getPost('from_date');
        $to_date = $this->request->getPost('to_date');
        $html = '';
        
        $patient = $this->patient->find($id);
        if (isset($patient)){
            $medhistory = $this->medhistory->getMedhistoryFromDateRange($id, $from_date, $to_date);
            
            if (!empty($medhistory) && is_array($medhistory)){
                foreach ($medhistory as $keys=>$medhistory_item){
                    
                    if(!empty($medhistory_item['pressuresys']) && !empty($medhistory_item['pressuredia'])){
                        $pressure = 'SYS/mmhg ' . $medhistory_item['pressuresys'] . ' &nbsp;&nbsp;DIA/mmhg ' . $medhistory_item['pressuredia'];
                    }else{
                        $pressure = 'ไม่มีข้อมูล';
                    }
                    
                    if(!empty($medhistory_item['pulse'])){
                        $pulse = $medhistory_item['pulse'] . ' PUL/min';
                    }else{
                        $pulse = 'ไม่มีข้อมูล';
                    }
                    
                    if(!empty($medhistory_item['temperature'])){
                        $temperature = $medhistory_item['temperature'] . ' องศาเซลเซียส';
                    }else{
                        $temperature = 'ไม่มีข้อมูล';
                    }
                    
                    if(!empty($medhistory_item['bmi'])){
                        $bmi = $medhistory_item['bmi'];
                    }else{
                        $bmi = 'ไม่มีข้อมูล';
                    }
                    
                    $html .= '<div id="medhistory_info_">
                                    <div class="container border border-secondary p-3">
                                        <div class="row form-group col-md-12">
                                            <div class="col-md-4"><b>วันที่บันทึก : </b>'. $medhistory_item["date"] .'</div>
                                            <div class="col-md-4"><b>บันทึกโดย : </b>'. $medhistory_item["name"] . ' ' . $medhistory_item["lastname"] .'</div>
                                        </div>
                                        <div class="row form-group col-md-12">
                                            <div class="col-md-4"><b>อายุ : </b>'. $medhistory_item["age"] .' ปี</div>
                                            <div class="col-md-2"><b>น้ำหนัก : </b>'. $medhistory_item["weight"] .' กิโลกรัม</div>
                                            <div class="col-md-3"><b>ส่วนสูง : </b>'. $medhistory_item["height"] .' เซนติเมตร</div>
                                        </div>
                                        <div class="row form-group col-md-12">
                                            <div class="col-md-4"><b>ความดัน : </b>'. $pressure .'</div>
                                            <div class="col-md-4"><b>ชีพจร : </b>'. $pulse .'</div>
                                        </div>
                                        <div class="row form-group col-md-12">
                                            <div class="col-md-4"><b>อุณหภูมิ : </b>'. $temperature .'</div>
                                            <div class="col-md-2"><b>BMI : </b>'. $bmi .'</div>
                                        </div>
                                        <div class="row form-group col-md-12">
                                            <div class="col-md-12"><b>คำอธิบายการรักษา</b></div>
                                            <div class="col-md-10">'. $medhistory_item["description"] .'</div>
                                        </div>';
                    
                    if(isset($medhistory_item['image']) && !empty($medhistory_item['image'])){
                        $html .= '<div class="row form-group col-md-12">
                                                <button class="btn btn-outline-primary" type="button" data-toggle="collapse" data-target="#image-search-'. $keys .'" aria-expanded="false" aria-controls="image-search-'. $keys .'">
                                                    รูปภาพประกอบการรักษา
                                                </button>
                                                <div class="col-md-10 collapse" id="image-search-'. $keys .'">
                                                    <br>';
                        $path = unserialize($medhistory_item['image']);
                        foreach ($path as $path_item){
                            $html .= '<img src="'. base_url() . $path_item .'" onclick="image_modal(this)" style="max-width: 100px; max-height: 100px; margin: 0px 3px;" id="myImg"/>';
                        }
                        $html .='</div>
                                    </div>';
                    }
                    $html .= '</div>
                                    <br>
                                </div>';
                }
            }else{
                $html = '<div class="container border border-secondary p-3">
                    <div class="text-center">ไม่พบประวัติที่ค้นหา</div>
                </div>
                <br>';
            }
            return $html;
        }else{
            session()->setFlashdata('no-data', 'ไม่พบข้อมูล ข้อมูลนี้อาจถูกลบไปแล้ว');
            return redirect()->to('/');
        }
    }
    
    public function create(){

//        write_file(APPPATH."log/log.txt", strval(123));
    
        if ($this->request->getMethod() === 'post'){
            $rules = [
                'citizennum' => [
                    'label' => 'citizennum',
                    'rules' => 'is_unique[patient.citizennum]|min_length[13]|max_length[13]|is_natural|permit_empty',
                    'errors' => [
                        'is_unique' => 'มีหมายเลขประจำตัวประชาชนนี้ในระบบแล้ว',
                        'min_length' => 'หมายเลขบัตรประชาชนไม่ถูกต้อง',
                        'max_length' => 'หมายเลขบัตรประชาชนไม่ถูกต้อง',
                        'is_natural' => 'กรุณากรอกเลขประจำตัวประชาชนเป็นหมายเลข'
                    ]
                ],
                'phone' =>[
                    'label' => 'phone',
                    'rules' => 'is_natural|permit_empty',
                    'errors' => [
                        'is_natural' => 'กรณากรอกเบอร์โทรศัพท์เป็นหมายเลข'
                    ]
                ],
                'zipcode' =>[
                    'label' => 'zipcode',
                    'rules' => 'is_natural',
                    'errors' => [
                        'is_natural' => 'กรณากรอกรหัสไปรษณีย์เป็นหมายเลข'
                    ]
                ]
            ];
            if ($this->validate($rules)){
                $this->patient->save([
                    'citizennum' => (!empty($this->request->getPost('citizennum')) ? $this->request->getPost('citizennum') : null),
                    'hospitalnum' =>$this->request->getPost('hospitalnum'),
                    'name' => $this->request->getPost('name'),
                    'lastname' => $this->request->getPost('lastname'),
                    'sex' => $this->request->getPost('sex'),
                    'marriage' =>$this->request->getPost('marriage'),
                    'blood' => $this->request->getPost('blood'),
                    'rh' =>$this->request->getPost('rh'),
                    'allergic' => (!empty($this->request->getPost('allergic')) ? $this->request->getPost('allergic') : null),
                    'bdate' => $this->request->getPost('bdate'),
                    'phone' => (!empty($this->request->getPost('phone')) ? $this->request->getPost('phone') : null),
                    'address' => $this->request->getPost('address'),
                    'province' => $this->request->getPost('province'),
                    'district' => $this->request->getPost('district'),
                    'subdistrict' => $this->request->getPost('subdistrict'),
                    'zipcode' => $this->request->getPost('zipcode')
                ]);
                session()->setFlashdata('success', 'เพิ่มข้อมูลผู้ป่วยสำเร็จแล้ว');
                 return redirect()->to('/');
                 
            }else {
                $data['validation'] =  $this->validator;
            }
        }
        $data['province'] = $this->province->getProvince();

        echo view('templates/header', ['title' => 'เพิ่มข้อมูลผู้ป่วย']);
        echo view('templates/navbar');
        echo view('patient/create', $data);
        echo view('templates/footer');
    }
    
    public function edit($id) {
        if(!empty($id)){
            $data = [
                'patient' => $this->patient->find($id),
                'title' => 'แก้ไขข้อมูลผู้ป่วย'
            ];
        }
        if (isset($data['patient'])){
             if ($this->request->getMethod() === 'post'){
                $rules = [
                    'citizennum' => [
                        'label' => 'citizennum',
                        'rules' => 'is_unique[patient.citizennum,id,'.$id.']|min_length[13]|max_length[13]|is_natural|permit_empty',
                        'errors' => [
                            'is_unique' => 'มีหมายเลขประจำตัวประชาชนนี้ในระบบแล้ว',
                            'min_length' => 'หมายเลขบัตรประชาชนไม่ถูกต้อง',
                            'max_length' => 'หมายเลขบัตรประชาชนไม่ถูกต้อง',
                            'is_natural' => 'กรุณากรอกเลขประจำตัวประชาชนเป็นหมายเลข'
                        ]
                    ],
                    'phone' =>[
                        'label' => 'phone',
                        'rules' => 'is_natural|permit_empty',
                        'errors' => [
                            'is_natural' => 'กรณากรอกเบอร์โทรศัพท์เป็นหมายเลข'
                        ]
                    ],
                    'zipcode' =>[
                        'label' => 'zipcode',
                        'rules' => 'is_natural',
                        'errors' => [
                            'is_natural' => 'กรณากรอกรหัสไปรษณีย์เป็นหมายเลข'
                        ]
                    ]
                ];
                if ($this->validate($rules)){
                        $this->patient->save([
                            'id' => $id,
                            'citizennum' => (!empty($this->request->getPost('citizennum')) ? $this->request->getPost('citizennum') : null),
                            'hospitalnum' =>$this->request->getPost('hospitalnum'),
                            'name' => $this->request->getPost('name'),
                            'lastname' => $this->request->getPost('lastname'),
                            'sex' => $this->request->getPost('sex'),
                            'marriage' =>$this->request->getPost('marriage'),
                            'blood' => $this->request->getPost('blood'),
                            'rh' =>$this->request->getPost('rh'),
                            'allergic' => (!empty($this->request->getPost('allergic')) ? $this->request->getPost('allergic') : null),
                            'bdate' => $this->request->getPost('bdate'),
                            'phone' => (!empty($this->request->getPost('phone')) ? $this->request->getPost('phone') : null),
                            'address' => $this->request->getPost('address'),
                            'province' => $this->request->getPost('province'),
                            'district' => $this->request->getPost('district'),
                            'subdistrict' => $this->request->getPost('subdistrict'),
                            'zipcode' => $this->request->getPost('zipcode')
                        ]);
                        session()->setFlashdata('success', 'แก้ไข้อมูลผู้ป่วยสำเร็จแล้ว');
                        return redirect()->to('/');
                 }else {
                $data['validation'] =  $this->validator;
                }
             }
            $data['province'] = $this->province->getProvince();

            echo view('templates/header', $data);
            echo view('templates/navbar');
            echo view('patient/edit', $data);
            echo view('templates/footer');
        }else{
            session()->setFlashdata('no-data', 'ไม่พบข้อมูล ข้อมูลนี้อาจถูกลบไปแล้ว');
            return redirect()->to('/');
        }
    }
    
    public function delete($id) {
        
        if(!empty($id)){
            $this->patient->delete($id);
        }
        return redirect()->to('/');
    }
    
    public function create_medhistory(){
        
        if ($this->request->getMethod() === 'post'){
            $data['patient'] = $this->patient->find($this->request->getPost('patient_id'));
            if (isset($data['patient'])){
                $rules = [
                    'weight' =>[
                        'label' => 'weight',
                        'rules' => 'decimal',
                        'errors' => [
                            'decimal' => 'กรณากรอกน้ำหนักเป็นหมายเลข'
                        ]
                    ],
                    'height' =>[
                        'label' => 'height',
                        'rules' => 'decimal',
                        'errors' => [
                            'decimal' => 'กรณากรอกส่วนสูงเป็นหมายเลข'
                        ]
                    ],
                    'temperature' =>[
                        'label' => 'temperature',
                        'rules' => 'decimal|permit_empty',
                        'errors' => [
                            'decimal' => 'กรณากรอกอุณหภูมิเป็นหมายเลข'
                        ]
                    ],
                    'bmi' =>[
                        'label' => 'bmi',
                        'rules' => 'decimal|permit_empty',
                        'errors' => [
                            'decimal' => 'กรณากรอก BMI เป็นหมายเลข'
                        ]
                    ],
                ];
                if ($this->validate($rules)){
                    $date = date('Y-m-d H:i:s ',time());
                    $savedata = [
                        'date' => $date,
                        'pressuresys' =>(!empty($this->request->getPost('pressuresys')) ? $this->request->getPost('pressuresys') : null),
                        'pressuredia' => (!empty($this->request->getPost('pressuredia')) ? $this->request->getPost('pressuredia') : null),
                        'pulse' => (!empty($this->request->getPost('pulse')) ? $this->request->getPost('pulse') : null),
                        'temperature' => (!empty($this->request->getPost('temperature')) ? $this->request->getPost('temperature') : null),
                        'height' =>$this->request->getPost('height'),
                        'weight' => $this->request->getPost('weight'),
                        'age' =>$this->request->getPost('age'),
                        'bmi' => (!empty($this->request->getPost('bmi')) ? $this->request->getPost('bmi') : null),
                        'description' => nl2br($this->request->getPost('description')),
                        'patient_id' => $this->request->getPost('patient_id'),
                        'user_id' => session()->get('id')
                    ];

                    if (isset($_FILES['image']['name'][0]) && !empty($_FILES['image']['name'][0])) {
                        $images = $this->request->getFileMultiple('image');
                        foreach($images as $keys=>$image){
                            $newName = $image->getRandomName();
                            $image->move(ROOTPATH . 'public/uploads/' . $this->request->getPost('patient_id'), $newName);
                            //$image->move(ROOTPATH . '../public_html/uploads/' . $this->request->getPost('patient_id'), $newName);
                            $imagedata[$keys] = '/uploads/' . $this->request->getPost('patient_id') . '/' . $newName;
                        }
                        $savedata['image'] = serialize($imagedata);
                    }
                    $test = $this->medhistory->save($savedata);
                    $session = session();
                    $session->setFlashdata('success-medhistory', 'เพิ่มบันทึกการรักษาสำเร็จแล้ว');
                    return redirect()->to('/view/' . $this->request->getPost('patient_id'));
                }else {
                    session()->setFlashdata('danger-valid', $this->validator->listErrors());
                }
            }else{
                session()->setFlashdata('no-data', 'ไม่พบข้อมูล ข้อมูลนี้อาจถูกลบไปแล้ว');
                return redirect()->to('/');
            }
        }
        return redirect()->to('/view/' . $this->request->getPost('patient_id'));
 
    }
    
    public function getDistrict ($id){

        return json_encode($this->address->district($id)); 
    }
    
    public function getSubdistrict ($id){

        return json_encode($this->address->subdistrict($id)); 
    }
    
    public function setPatientforpdf($patient) {
        
        $patient['bdate'] = strtotime($patient['bdate']); $patient['bdate'] = date('d/m/Y', $patient['bdate']);
        if ($patient['sex'] == '0'){
            $patient['sex'] = 'ชาย';
        }else{
            $patient['sex'] = 'หญิง';
        }
        
        if ($patient['blood'] == '0'){
            $patient['blood'] = 'O';
        }elseif ($patient['blood'] == '1'){
            $patient['blood'] = 'A';
        }elseif ($patient['blood'] == '2'){
            $patient['blood'] = 'B';
        }else{
            $patient['blood'] = 'ABO';
        }

        if ($patient['rh'] == '0'){
            $patient['rh'] = 'Rh+';
        }else{
            $patient['rh'] = 'Rh-';
        }

        if (empty($patient['allergic'])){
            $patient['allergic'] = 'ไม่มี';
        }

        if (empty($patient['phone'])){
            $patient['phone'] = 'ไม่มี';
        }

        if ($patient['marriage'] == '0'){
            $patient['marriage'] = 'โสด';
        }elseif ($patient['marriage'] == '1'){
            $patient['marriage'] = 'สมรส';
        }elseif ($patient['marriage'] == '2'){
            $patient['marriage'] = 'หม้าย';
        }elseif ($patient['marriage'] == '3'){
            $patient['marriage'] = 'หย่า';
        }else{
            $patient['marriage'] = 'แยกกันอยู่';
        }
        
        return $patient;
    }
    
    public function pdf($id) {
        
        $patient = $this->patient->find($id);

        if (isset($patient)){
            $patient = $this->setPatientforpdf($patient);
            $address = $this->address->getAddress($patient['province'], $patient['district'], $patient['subdistrict']);
            $medhistory = $this->medhistory->getMedhistoryFromPatientId($id);
            $date = date('d/m/Y');

            $this->response->setHeader('Content-Type', 'application/pdf'); 
            $this->pdf->SetCreator(PDF_CREATOR);
            $this->pdf->SetAuthor('Doctor');
            $this->pdf->SetTitle('ประวัติการรักษา | ' . $patient['name'] . ' ' . $patient['lastname']);
            $this->pdf->SetSubject('TCPDF Tutorial');
            $this->pdf->SetKeywords('TCPDF, PDF, example, test, guide');

            // set default monospaced font
            $this->pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

            // set margins
            $this->pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
            $this->pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
            $this->pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

            // set auto page breaks
            $this->pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

            // ---------------------------------------------------------

            // set font
            $this->pdf->SetFont('thsarabun', '', 16);

            // add a page
            $this->pdf->AddPage();

            $html = <<<EOD
            <div style="text-align: right;"><b>ออกเมื่อวันที่ : </b>{$date}</div>
            <table width="100%" border="0">
                <tr>
                    <td><b>Hospital Number (HN) : </b>{$patient['hospitalnum']}</td>
                    <td width="330"><b>เลขที่ประจำตัวประชาชน : </b>{$patient['citizennum']}</td>
                </tr>
                <tr><td></td><td></td></tr>
                <tr>
                    <td><b>ชื่อ-นามสกุล : </b>{$patient['name']} {$patient['lastname']}</td>
                    <td><b>เพศ : </b>{$patient['sex']}</td>
                </tr>
                <tr><td></td><td></td></tr>
                <tr>
                    <td><b>กรุ๊ปเลือด : </b>{$patient['blood']} {$patient['rh']}</td>
                    <td><b>ประวัติการแพ้ยา : </b>{$patient['allergic']}</td>
                </tr>
                <tr><td></td><td></td></tr>
                <tr>
                    <td><b>วันเกิด : </b>{$patient['bdate']}</td>
                    <td><b>สถานภาพสมรส : </b>{$patient['marriage']}</td>
                </tr>
                <tr><td></td><td></td></tr>
                <tr>
                    <td><b>ที่อยู่ : </b>{$patient['address']} {$address['subdistrict']} {$address['district']} {$address['province']} {$patient['zipcode']}</td>
                    <td><b>เบอร์โทรศัพท์ : </b>{$patient['phone']}</td>
                </tr>
            </table>
            <br>
            <h2><u>ประวัติการรักษา</u></h2>
            <hr>
EOD;
            if (!empty($medhistory) && is_array($medhistory)){
                foreach ($medhistory as $medhistory_item){
                $html .= <<<EOD
                    <table style="width: 100%;" nobr="true">
                        <tbody>
                        <tr><td></td><td></td><td></td></tr> 
                            <tr>
                                <td width="240"><b>วันที่บันทึก : </b>{$medhistory_item['date']}</td>
                                <td width="120"><b>บันทึกโดย : </b>{$medhistory_item['name']} {$medhistory_item['lastname']}</td>
                                <td width="120"></td>
                            </tr>
                            <tr><td></td><td></td><td></td></tr> 
                            <tr>
                                <td><b>อายุ : </b>{$medhistory_item['age']}</td>
                                <td><b>น้ำหนัก : </b>{$medhistory_item['weight']} กิโลกรัม</td>
                                <td width><b>ส่วนสูง : </b>{$medhistory_item['height']} เซนติเมตร</td>
                            </tr>
                            <tr><td></td><td></td><td></td></tr> 
                            <tr>
                                <td><b>ความดัน : </b> SYS/mmhg {$medhistory_item['pressuresys']} &nbsp; &nbsp; DIA/mmhg {$medhistory_item['pressuredia']}</td>
                                <td><b>ชีพจร : </b>{$medhistory_item['pulse']} PUL/min</td>
                                <td></td>
                            </tr>
                            <tr><td></td><td></td><td></td></tr> 
                            <tr>
                                <td><b>อุณหภูมิ : </b>{$medhistory_item['temperature']} องศาเซลเซียส</td>
                                <td><b>BMI : </b>{$medhistory_item['bmi']}</td>
                                <td></td>
                            </tr>
                            <tr><td></td><td></td><td></td></tr> 
                            <tr>
                                <td><b>คำอธิบายการรักษา</b></td>
                            </tr>
                            <tr>
                                <td width="480">{$medhistory_item['description']}</td>
                            </tr>
                        </tbody>
                    </table>
                <br>
                <hr>
EOD;
                }
            }else{
                $html .= <<<EOD
                <div style="text-align: center;"><i>ไม่มีประวัติการรักษา</i></div>
EOD;
            }

            $this->pdf->writeHTMLCell(0, 0, '', '', $html, 0, 1, 0, true, '', true);
            // ---------------------------------------------------------

            //Close and output PDF document
//            $filename = $patient['name'] . ' ' . $patient['lastname'].'.pdf';

            $this->pdf->Output($patient['hospitalnum'].'.pdf', 'I');
        }else{
            session()->setFlashdata('no-data', 'ไม่พบข้อมูล ข้อมูลนี้อาจถูกลบไปแล้ว');
            return redirect()->to('/');
        }
    }
            
}
