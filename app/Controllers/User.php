<?php

namespace App\Controllers;

use CodeIgniter\Controller;

class User extends BaseController {
    
    public function index() {
        
        $data['title'] = 'เข้าสู่ระบบ';
        helper(['form']);
        
        if ($this->request->getMethod() == 'post'){
            $rules = [
            'username' => [
                'rules' => 'required',
                'errors' => [
                    'required' => 'กรุณากรอกชื่อผู้ใช้'
                ]
            ],
            'password' => [
                'rules' => 'required|validateUser[username,password]',
                'errors' => [
                    'required' => 'กรุณากรอกรหัสผ่าน',
                    'validateUser' => 'ชื่อผู้ใช้หรือรหัสผ่านไม่ถูกต้อง'
                ]
            ],
        ];
            
            if (! $this->validate($rules)){
                $data['validation'] = $this->validator;
            }else{
                $user = $this->user->getUserfromUsername($this->request->getVar('username'));
                $this->setUserSession($user);
                 return redirect()->to('/');
            }
        }
        
        echo view('user/login', $data);
    }
    
    private function setUserSession($user) {
        $data = [
            'id' => $user['id'],
            'name' => $user['name'],
            'lastname' => $user['lastname'],
            'username' => $user['username'],
            'permission' => $user['permission'],
            'status' => $user['status'],
            'isLoggedIn' => true
        ];
        session()->set($data);
        
        return true;
    }
    
    public function register(){
        
        $data['title'] = 'สร้างบัญชีผู้ใช้';
        helper(['form']);
        
        if ($this->request->getMethod() == 'post'){
            $rules = [
            'name' => [
                'label' => 'name',
                'rules' => 'required',
                'errors' => [
                    'required' => 'กรุณากรอกชื่อ'
                ]
            ],
            'lastname' => [
                'label' => 'lastname',
                'rules' => 'required',
                'errors' => [
                    'required' => 'กรุณากรอกนามสกุล'
                ]
            ],
            'username' => [
                'label' => 'username',
				'rules' => 'required|min_length[4]|max_length[20]|is_unique[user.username]',
                'errors' => [
                    'required' => 'กรุณากรอกชื่อผู้ใช้',
					'min_length' => 'ชื่อผู้ใช้ต้องมีความยาว 4-20 ตัวอักษร',
                    'max_length' => 'ชื่อผู้ใช้ต้องมีความยาว 4-20 ตัวอักษร',
                    'is_unique' => 'ชื่อผู้ใช้ซ้ำ'
                ]
            ],
            'password' => [
                'label' => 'password',
                'rules' => 'required|min_length[8]|max_length[20]',
                'errors' => [
                    'required' => 'กรุณากรอกรหัสผ่าน',
					'min_length' => 'รหัสผ่านต้องมีความยาว 8-20 ตัวอักษร',
                    'max_length' => 'รหัสผ่านต้องมีความยาว 8-20 ตัวอักษร'
                ]
            ],
            'password_confirm' => [
                'label' => 'password_confirm',
                'rules' => 'matches[password]',
                'errors' => [
                    'matches' => 'ยืนยันรหัสผ่านไม่ถูกต้อง'
                ]
            ]
        ];
            
            if (! $this->validate($rules)){
                $data['validation'] = $this->validator;
            }else{
                 $this->user->save([
                     'name' => $this->request->getVar('name'),
                     'lastname' => $this->request->getVar('lastname'),
                     'username' => $this->request->getVar('username'),
                     'password' => $this->request->getVar('password')
                 ]);
                 session()->setFlashdata('success', 'สร้างบัญชีผู้ใช้สำเร็จแล้ว');
                 
                 return redirect()->to('/login');
            }
        }
                  
        echo view('user/register', $data);
    }
    
    public function profile() {
        
        $data['title'] = 'Update';
        helper(['form']);
        
         if ($this->request->getMethod() == 'post'){
            $rules = [
                'name' => [
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'กรุณากรอกชื่อ'
                    ]
                ],
                'lastname' => [
                    'rules' => 'required',
                    'errors' => [
                        'required' => 'กรุณากรอกนามสกุล'
                    ]
                ],
                'username' => [
                    'rules' => 'required|min_length[4]|max_length[20]|is_unique[user.username,id,'.session()->get('id').']',
                    'errors' => [
                        'required' => 'กรุณากรอกชื่อผู้ใช้',
						'min_length' => 'ชื่อผู้ใช้ต้องมีความยาว 4-20 ตัวอักษร',
						'max_length' => 'ชื่อผู้ใช้ต้องมีความยาว 4-20 ตัวอักษร',
                        'is_unique' => 'ชื่อผู้ใช้ซ้ำ'
                    ]
                ]
            ];
            
            if ($this->request->getPost('password') != ''){
                $rules['password'] = [
                    'rules' => 'required|min_length[8]|max_length[20]',
                    'errors' => [
                        'required' => 'กรุณากรอกรหัสผ่าน',
						'min_length' => 'รหัสผ่านต้องมีความยาว 8-20 ตัวอักษร',
						'max_length' => 'รหัสผ่านต้องมีความยาว 8-20 ตัวอักษร'
                    ]
                    
                ];
                $rules['password_confirm'] = [
                    'rules' => 'matches[password]',
                    'errors' => [
                        'matches' => 'ยืนยันรหัสผ่านไม่ถูกต้อง'
                    ]
                ];
            }
            
            if (! $this->validate($rules)){
                $data['validation'] = $this->validator;
            }else{
                $newData = [
                    'id' => session()->get('id'),
                     'name' => $this->request->getPost('name'),
                     'lastname' => $this->request->getPost('lastname'),
                     'username' => $this->request->getPost('username'),
                 ];
                 if ($this->request->getPost('password') != ''){
                     $newData['password'] = $this->request->getPost('password');
                 }
                 
                 $this->user->save($newData);
                 session()->setFlashdata('profile-success', 'แก้ไขข้อมูลส่วนตัวสำเร็จแล้ว');
                 
                 $user = $this->user->getUserfromUsername($this->request->getPost('username'));
                 $this->setUserSession($user);
                 return redirect()->to('/profile');
            }
        }
        $data['user'] = $this->user->find(session()->get('id'));
        
        echo view('templates/header', $data);
        echo view('templates/navbar');
        echo view('user/profile', $data);
        
    }
    
    public function approve($status=false, $id = false) {
        
        $data['title'] = 'รายชื่อบัญชีที่รอการอนุมัติ';
        
        if ($status === '0'){
            $data = $this->user->find($id);
            if($data['status'] == '2'){
                $this->user->save([
                'id' => $id,
                'status' => $status
            ]);
            session()->setFlashdata('approve-success', 'อนุมัติบัญชีผู้ใช้ของ ' . $data['name'] . ' สำเร็จแล้ว');
            return redirect()->to('/approve');
            
            }else{
            return redirect()->to('/');
            }
        }
        
        $data['new_user'] = $this->user->getPendingUser();
        
        echo view('templates/header', $data);
        echo view('templates/navbar');
        echo view('user/approve', $data);
        echo view('templates/footer');
    }
    
    public function deleteUser($id) {
        $data = $this->user->find($id);
            if($data['status'] == '2'){
                if(!empty($id)){
                    $this->user->delete($id);
                }
                session()->setFlashdata('approve-danger', 'ปฏิเสธบัญชีผู้ใช้ของ ' . $data['name'] . ' สำเร็จแล้ว');
                return redirect()->to('/approve');
            }
    }
    
    public function mange() {
        
        $data['title'] = 'จัดการสถานะบัญชีผู้ใช้';

        if ($this->request->getMethod() == 'post'){
            
            $users = $this->user->getUser();
            
            foreach ($users as $user) {
                $this->user->save([
                    'id' => $user['id'],
                     'status' => $this->request->getPost('status_'.$user['id'])
                ]);
            }
            session()->setFlashdata('manage-success', 'บันทึกสถานะของบัญชีผู้ใช้ทั้งหมดสำเร็จแล้ว');
        }
        
        $data['user'] = $this->user->getUser();
        
        echo view('templates/header', $data);
        echo view('templates/navbar');
        echo view('user/mange', $data);
        echo view('templates/footer');
    }
    
    public function logout(){
        
        session()->destroy();
        
        return redirect()->to('/login');
    }
}
