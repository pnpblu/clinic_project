<?php namespace App\Filters;

use CodeIgniter\HTTP\RequestInterface;
use CodeIgniter\HTTP\ResponseInterface;
use App\Models\UserModel;
use CodeIgniter\Filters\FilterInterface;


class StatusCheck implements FilterInterface
{
    public function before(RequestInterface $request, $arguments = null)
    {
        $model = new UserModel;
        $user = $model->where('id', session()->get('id'))
                                                  ->first();
        if ($user['status'] == '1'){
            
            session()->remove('isLoggedIn');
            session()->setFlashdata('danger', 'ไม่สามารถใช้งานได้ เนื่องจากบัญชีผู้ใช้ของคุณถูกระงับแล้ว');
            return redirect()->to('/login');
            
        }elseif ($user['status'] == '2'){
            
            session()->remove('isLoggedIn');
            session()->setFlashdata('danger', 'ไม่สามารถใช้งานได้ เนื่องจากบัญชีผู้ใช้ของคุณยังไม่ได้รับการอนุมัติ');
            return redirect()->to('/login');
        }
        
    }

    //--------------------------------------------------------------------

    public function after(RequestInterface $request, ResponseInterface $response, $arguments = null)
    {
        // Do something here
    }
}