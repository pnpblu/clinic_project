<?php
namespace App\Libraries;

use App\Models\ProvinceModel;
use App\Models\DistrictModel;
use App\Models\SubdistrictModel;

helper('filesystem');

class Address{
    
    public function district($id) {
        $model = new DistrictModel();
        
        return $model->getDistrict($id);
    }
    
    public function subdistrict($id){
        $model = new SubdistrictModel();

        return $model->getSubdistrict($id);
    }
    
    public function getAddress($id_province, $id_district, $id_subdistrict){
        $modelp = new ProvinceModel();
        $modeld = new DistrictModel();
        $models = new SubdistrictModel();
        
        $province = $modelp->getProvinceFromId($id_province);
        $district = $modeld->getDistrictFromId($id_district);
        $subdistrict = $models->getSubdistrictFromId($id_subdistrict);
        $data['province'] = $province['name_th'];
        $data['district'] = $district['name_th'];
        $data['subdistrict'] = $subdistrict['name_th'];
        
        return $data;
    }
    
    
}
