<?php

namespace App\Libraries;

use TCPDF;

class PDF extends TCPDF{

    public function Header()
    {
        $this->SetY(12);
        $this->SetFont('thsarabunb', '', 26);
        $this->Cell('', '', 'กาญจนาภิเษก-บางบอนคลินิก', 'B', 0, 'L');
    }
    public function footer() {
        $this->SetY(-12);
        $this->SetFont('thsarabun', '', 14);
        $this->Cell('', '', 'กาญจนาภิเษก-บางบอนคลินิก', 'T', 0, 'C');
        $this->Cell('', '', 'Page ' . $this->getAliasNumPage() . '/' . $this->getAliasNbPages(), 0, 0, 'R');
    }
    
}
