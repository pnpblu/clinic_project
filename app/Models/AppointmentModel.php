<?php
namespace App\Models;
use CodeIgniter\Model;

class AppointmentModel extends Model{
    protected $table = 'appointment';
    protected  $primaryKey = 'id';
    protected $allowedFields = ['date', 'description', 'patient_id'];
    
    public function getAppointmentJoinPatient() {
        return $this->orderBy('date', 'ASC')
                           ->join('patient', 'patient.id = appointment.patient_id')
                           ->findAll();
    }
    
    public function getAppointment(){
        return $this->orderBy('date', 'ASC')
                            ->findAll();
    }
}
