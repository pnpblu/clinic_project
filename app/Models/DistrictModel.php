<?php

namespace App\Models;
use CodeIgniter\Model;

class DistrictModel extends Model{
    
    protected $table = 'district';
    protected  $primaryKey = 'id';
    protected $allowedFields = ['code', 'name_th', 'name_en', 'province_id'];

    public function getDistrict($province_id){
        return $this->asArray()
                            ->where(['province_id' => $province_id])
                            ->findAll();
    }
    
    public function getDistrictFromId($id){
        return $this->asArray()
                            ->where(['id' => $id])
                            ->first();
    }
}
