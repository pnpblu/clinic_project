<?php
namespace App\Models;
use CodeIgniter\Model;

class MedHistoryModel extends Model{
    protected $table = 'medhistory';
    protected  $primaryKey = 'id';
    protected $allowedFields = ['date', 'pressuresys', 'pressuredia', 'pulse', 'temperature', 'height', 'weight', 'age', 'bmi', 'description', 'image', 'patient_id', 'user_id'];
    
    public function getMedhistoryFromPatientId($patient_id) {
        return $this->where('patient_id', $patient_id)
                            ->join('user', 'user.id = medhistory.user_id')
                            ->orderBy('date', 'DESC')
                            ->findAll();
    }
    
    public function getMedhistoryFromDateRange($patient_id, $from_date, $to_date){
        return $this->where('patient_id', $patient_id)
                            ->where('date BETWEEN "'. $from_date . ' 00:00:00.000" AND "' . $to_date . ' 23:59:59.000"')
                            ->join('user', 'user.id = medhistory.user_id')
                            ->orderBy('date', 'DESC')
                            ->findAll();
    }
    
}
