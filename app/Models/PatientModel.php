<?php
namespace App\Models;
use CodeIgniter\Model;

class PatientModel extends Model{
    protected $table = 'patient';
    protected  $primaryKey = 'id';
    protected $allowedFields = ['citizennum', 'hospitalnum', 'name', 'lastname', 'sex', 'marriage', 'blood', 'rh', 'allergic', 'bdate', 'phone', 'address', 'province', 'district', 'subdistrict', 'zipcode'];

    public function getPatient(){
        return $this->findAll();
    }
    
}
