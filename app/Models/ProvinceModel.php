<?php
namespace App\Models;
use CodeIgniter\Model;

class ProvinceModel extends Model{
    protected $table = 'province';
    protected  $primaryKey = 'id';
    protected $allowedFields = ['code', 'name_th', 'name_en'];

    public function getProvince(){
            return $this->findAll();
    }
    
    public function getProvinceFromId($id){
        return $this->asArray()
                            ->where(['id' => $id])
                            ->first();
    }
    
}