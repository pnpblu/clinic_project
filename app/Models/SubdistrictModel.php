<?php

namespace App\Models;
use CodeIgniter\Model;

class SubdistrictModel extends Model{
    
    protected $table = 'subdistrict';
    protected  $primaryKey = 'id';
    protected $allowedFields = ['zip_code', 'name_th', 'name_en', 'district_id'];

    public function getSubdistrict($district_id){
        return $this->asArray()
                            ->where(['district_id' => $district_id])
                            ->findAll();
    }
    
    public function getSubdistrictFromId($id){
        return $this->asArray()
                            ->where(['id' => $id])
                            ->first();
    }
}
