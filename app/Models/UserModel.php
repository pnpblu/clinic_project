<?php

namespace App\Models;

use CodeIgniter\Model;

class UserModel extends Model {
    protected $table = 'user';
    protected  $primaryKey = 'id';
    protected $allowedFields = ['username', 'password', 'name', 'lastname', 'permission', 'status'];
    protected $beforeInsert = ['beforeInsert'];
    protected $beforeUpdate = ['beforeUpdate'];


    protected function beforeInsert(array $data){
        $data = $this->passwordHash($data);
        
        return $data;
    }
    
    protected function beforeUpdate(array $data){
        $data = $this->passwordHash($data);
        
        return $data;
    }
    
    protected function passwordHash(array $data) {
        if(isset($data['data']['password'])){
            $data['data']['password'] = password_hash($data['data']['password'], PASSWORD_DEFAULT);

            return $data;
        }
        return $data;
    }

    public function checkAuth($username, $password){
        return $this->where(['username' => $username,
                                           'password' => $password])
                            ->first();
        
    }
    
    public function getUserfromUsername($username) {
        return $this->where('username', $username)
                            ->first();
    }
    
    public function getPendingUser() {
        return $this->where('status', '2')
                          ->findAll();
    }
    
    public function getUser() {
        return $this->where('permission', '1')
                             ->whereIn('status', ['0', '1'])
                             ->findAll();
    }
}
