<div class="container border p-3">
    <h2><?= esc($title); ?></h2>
    <?php if (session()->get('success-appointment-edit')): ?>
    <div class="alert alert-success" role="alert">
        <?= session()->get('success-appointment-edit'); ?>
    </div>
    <?php elseif (session()->get('appoint-no-data')): ?>
    <div class="alert alert-success" role="alert">
        <?= session()->get('appoint-no-data'); ?>
    </div>
    <?php endif; ?>
    <hr>
    <div id="calendar" style="margin:0 auto"></div>
    <br>
    <hr>
    
    <div class="border border-secondary p-3 col-md-5" id="appointmentEdit" hidden>
        <h5 id="editTitle">แก้ไขการนัดหมายของ</h5>
        <hr>
        <form action="/appointment/edit" method="post" class="needs-validation" novalidate>
            <div class="row form-group col-md-12">
                <input type="hidden" name="id" value=""/>
                <label class="control-label col-md-4" for="date">วันที่ </label>
                <input type="date" class="form-control col-md-6" name="date" value="<?= set_value('date') ?>" required/>
            </div>
            <div class="row form-group col-md-12">
                <label class="control-label col-md-4" for="hour">เวลา </label>
                <input type="text" class="form-control col-md-3" name="hour" placeholder="ชั่วโมง" value="<?= set_value('hour') ?>" required/><p style="padding: 6px 12px; margin-bottom: 0px;">:</p>
                <input type="text" class="form-control col-md-3" name="minute" placeholder="นาที" value="<?= set_value('minute') ?>" required/><p style="padding: 6px 12px; margin-bottom: 0px;">น.</p>
            </div>
            <div class="row form-group col-md-12" >
                <label class="control-label col-md-4" for="description">รายละเอียดการนัด </label>
                <textarea class="form-control col-md-8" rows="3" name="description" placeholder="รายละเอียดการนัด" value="<?= set_value('description') ?>" required></textarea>
            </div>
            <div class="row form-group col-md-12" style="margin-bottom: 0px">
                <button type="submit" class="btn btn-success mx-1">บันทึก</button>
                <button type="reset" class="btn btn-danger mx-1" id="cancelEdit">ยกเลิก</button>
            </div>
        </form>
    </div>
    
    <table id="allappointment" class="table table-striped table-hover" style="width: 100%">
        <thead>
            <tr>
                <th class="text-left" >วัน/เวลาที่นัดหมาย</th>
                <th class="text-left" >ชื่อ-นามสกุล</th>
                <th class="text-left" >รายละเอียดการนัด</th>
                <th class="text-right" ></th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($appointment) && is_array($appointment)) :
                $index = 0;
                foreach ($appointment as $appointment_item): ?>
            <tr>
                <td class="text-left"><?php $date = new DateTime($appointment_item['date']); echo $date->format('d/m/Y H:i'); ?></td>
                <td class="text-left"><?= $appointment_item['name'] .' '. $appointment_item['lastname']; ?></td>
                <td class="text-left"><?= $appointment_item['description']; ?></td>
                <td class="text-right">
                    <button type="button" class="btn btn-sm btn-info mx-1" data-id="<?= $appointment_item['id'];?>" data-index="<?= $index;?>">แก้ไข</button>
                    <button type="button" class="btn btn-sm btn-danger mx-1" data-href="<?= $appointment_item['id']; ?>" data-toggle="modal" data-target="#confirm-delete">ลบ</button>
                </td>
            </tr>
            <?php $index++;
            endforeach; 
            else : ?>
            <tr>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center">ไม่มีข้อมูลการนัดหมาย</td>
                <td class="text-center"></td>
            </tr>
            <?php endif; ?>
        </tbody>
    </table>
    
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="mymodalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    ยืนยันการลบ
                </div>
                <div class="modal-body">
                    ต้องการลบใช่หรือไม่ ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                    <a class="btn btn-danger btn-ok appointmentClickDelete">ยืนยัน</a>
                </div>
            </div>
        </div>
    </div>
</div>