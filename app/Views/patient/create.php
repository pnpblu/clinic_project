<div class="container border p-3">
    <h2><?= esc($title); ?></h2>
    <hr>
    <?php if (isset($validation)): ?>
        <div class="col-12">
            <div class="alert alert-danger" role="alert">
                <?= $validation->listErrors(); ?>
            </div>
        </div>
    <?php endif; ?>

    <form action="/create" method="post" class="needs-validation" novalidate>
        <label class="control-label col-md-3" style="color: red">ห้ามเว้นว่างช่องที่มี *</label>
        <div class="form-group input-group">
            <label class="control-label col-md-2">Hospital Number (HN) </label>
            <input type="text" class="form-control col-md-4" name="hospitalnum" placeholder="Hospital Number (HN)" value="<?= set_value('hospitalnum') ?>" required/>
            <label class="control-label " style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">ชื่อ </label>
            <input type="text" class="form-control col-md-4" name="name" placeholder="ชื่อ" value="<?= set_value('name') ?>" required/>
            <label class="control-label " style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">นามสกุล </label>
            <input type="text" class="form-control col-md-4" name="lastname" placeholder="นามสกุล" value="<?= set_value('lastname') ?>" required/>
            <label class="control-label " style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2" >เพศ </label>
            <select class="form-control col-md-2" name="sex" value="" required>
                <option hidden selected></option>
                <option value="0" <?= set_select('sex', '0') ?>>ชาย</option>
                <option value ="1" <?= set_select('sex', '1') ?>>หญิง</option>
            </select>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">สถานภาพสมรส </label>
            <select class="form-control col-md-2" name="marriage" value="" required>
                <option hidden selected></option>
                <option value="0" <?= set_select('marriage', '0') ?>>โสด</option>
                <option value ="1" <?= set_select('marriage', '1') ?>>สมรส</option>
                <option value ="2" <?= set_select('marriage', '2') ?>>หม้าย</option>
                <option value ="3" <?= set_select('marriage', '3') ?>>หย่า</option>
                <option value ="4" <?= set_select('marriage', '4') ?>>แยกกันอยู่</option>
            </select>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">กรุ๊ปเลือด </label>
            <select class="form-control col-md-1" name="blood" value="" required>
                <option hidden selected></option>
                <option value="0" <?= set_select('blood', '0') ?>>O</option>
                <option value ="1" <?= set_select('blood', '1') ?>>A</option>
                <option value ="2" <?= set_select('blood', '2') ?>>B</option>
                <option value ="3" <?= set_select('blood', '3') ?>>ABO</option>
            </select>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>

            <label class="control-label col-md-1">Rh </label>
            <select class="form-control col-md-1" name="rh" value="" required>
                <option hidden selected></option>
                <option value="0" <?= set_select('rh', '0') ?>>+</option>
                <option value ="1" <?= set_select('rh', '1') ?>>-</option>
            </select>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">ประวัติการแพ้ยา </label>
            <input type="text" class="form-control col-md-4" name="allergic" placeholder="ประวัติการแพ้ยา" value="<?= set_value('allergic') ?>"/>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">วัน/เดือน/ปี เกิด </label>
            <input type="date" name="bdate" max="<?= date('Y-m-d'); ?>" value="<?= set_value('bdate') ?>" required>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">เลขประจำตัวประชาชน </label>
            <input type="text" class="form-control col-md-2 number_textbox" name="citizennum" placeholder="เลขประจำตัวประชาชน" value="<?= set_value('citizennum') ?>" minlength="13" maxlength="13"/>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">เบอร์โทรศัพท์ </label>
            <input type="text" class="form-control col-md-2 number_textbox" name="phone" placeholder="เบอร์โทรศัพท์" value="<?= set_value('phone') ?>" />
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">ที่อยู่ </label>
            <textarea class="form-control col-md-4" name="address" placeholder="ที่อยู่" required><?= set_value('address') ?></textarea>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2" required>จังหวัด </label>
            <select class="form-control col-md-4" name="province" id="province" value="" required>
                <option value="">กรุณาเลือกจังหวัด</option>
                <?php if (!empty($province) && is_array($province)) :
                foreach ($province as $province_item): ?>
                <option value="<?= $province_item['id']; ?>" <?= set_select('province', '$province_item[\'id\']') ?>><?= $province_item['name_th']; ?></option>
                <?php endforeach; endif; ?>
            </select>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">เขต/อำเภอ </label>
            <select class="form-control col-md-4" name="district" id="district" value="" required>
                <option value="">กรุณาเลือกเขต/อำเภอ</option>
            </select>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">แขวง/ตำบล </label>
            <select class="form-control col-md-4" name="subdistrict" id="subdistrict" value="" required>
                <option value="">กรุณาเลือกแขวง/ตำบล</option>
            </select>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">รหัสไปรษณีย์ </label>
            <input type="text" class="form-control col-md-2 number_textbox" name="zipcode" placeholder="รหัสไปรษณีย์" value="<?= set_value('zipcode') ?>" required>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>
        </div>

        <div class="form-group input-group">
            <button type="submit" class="btn btn-success mx-1">บันทึก</button>
            <a  class="btn btn-danger" href="/">ย้อนกลับ</a>
        </div>
    </form>
</div>