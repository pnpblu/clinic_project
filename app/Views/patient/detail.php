<div class="container border p-3">
    <h2><?= esc($title); ?></h2>
    <hr>
    
    <div class="container">
        <div class="row form-group col-md-12">
            <div class="col-md-4">Hospital Number (HN) : <?= $patient['hospitalnum']; ?></div>
            <a href="/pdf/<?= $patient['id'] ?>" class="btn btn-link float-right" style="margin-left: auto;" target="_blank"><i class="fa fa-print" style="font-size:24px; color: black;"></i> พิมพ์ประวัติการรักษา</a>
        </div>
        <div class="row form-group col-md-12">
            <div class="col-md-4">ชื่อ-นามสกุล : <?= $patient['name'].' '.$patient['lastname']; ?></div>
            <div class="col-md-4">เพศ : <?php if($patient['sex'] == '0') : echo 'ชาย'; else : echo 'หญิง';endif; ?></div>
        </div>
        <div class="row form-group col-md-12">
            <div class="col-md-4">เลขประจำตัวประชาชน : <?= $patient['citizennum']; ?></div>
            <div class="col-md-4">กรุ๊ปเลือด : <?php if($patient['blood'] == '0') : echo 'O'; 
                                                                                elseif($patient['blood'] == '1') : echo 'A'; 
                                                                                elseif($patient['blood'] == '2') : echo 'B';
                                                                                else : echo 'ABO'; endif; 
                                                                                if($patient['rh'] == '0') : echo '  Rh+';
                                                                                else : echo '  Rh-'; endif;?>
            </div>
        </div>
        <div class="row form-group col-md-12">
            <div class="col-md-4">ประวัติการแพ้ยา : <?php if( empty($patient['allergic'])) : echo 'ไม่มี';
                                                                                        else : echo $patient['allergic']; endif;?></div>
            <?php $year = date_diff(date_create($patient['bdate']), date_create('today'))->y; 
                        $month = date_diff(date_create($patient['bdate']), date_create('today'))->m; 
                        $day = date_diff(date_create($patient['bdate']), date_create('today'))->d; 
                        $age = $year . ' ปี ' . $month . ' เดือน ' . $day .' วัน'; ?>
            <div class="col-md-4">อายุ : <?= $age; ?></div>
        </div>
        <div class="row form-group col-md-12">
            <div class="col-md-4">สถานภาพสมรส : <?php if($patient['marriage'] == '0') : echo 'โสด'; 
                                                                                elseif($patient['marriage'] == '1') : echo 'สมรส'; 
                                                                                elseif($patient['marriage'] == '2') : echo 'หม้าย';
                                                                                elseif($patient['marriage'] == '3') : echo 'หย่า'; 
                                                                                else : echo 'แยกกันอยู่'; endif; ?>
            </div>
            <div class="col-md-4">เบอร์โทรศัพท์ : <?php if( empty($patient['phone'])) : echo 'ไม่มี';
                                                                                        else : echo $patient['phone']; endif; ?></div>
        </div>
        <div class="row form-group col-md-12">
            <div class="col-md-8">ที่อยู่ : <?= $patient['address'] . ' ' . $address['subdistrict'] . ' ' . $address['district'] . ' จังหวัด' . $address['province'] . ' ' . $patient['zipcode']; ?></div>
        </div>

    </div>
    <hr>
    
    <?php if (session()->get('success-appointment')): ?>
    <div class="alert alert-success" role="alert">
        <?= session()->get('success-appointment'); ?>
    </div>
    <?php endif; ?>
    
    <button class="btn btn-info" type="button" data-toggle="collapse" data-target="#addAppointment" aria-expanded="false" aria-controls="addAppointment">
        เพิ่มการนัดหมายผู้ป่วย
    </button>
    <button class="btn btn-info" type="button" data-toggle="collapse" data-target="#create-med" aria-expanded="false" aria-controls="create-med">
        เพิ่มบันทึกการรักษา
    </button>
    <br>
    <br>
    <div class="border border-secondary p-3 col-md-5 collapse" id="addAppointment">
        <h5>นัดผู้ป่วย</h5>
        <hr>
        <form action="/appointment/create" method="post" class="needs-validation" novalidate>
            <div class="row form-group col-md-12">
                <input type="hidden" name="patient_id" value="<?= $patient['id']; ?>"/>
                <label class="control-label col-md-4" for="date">วันที่ </label>
                <input type="date" class="form-control col-md-6" name="date" min="<?= date('Y-m-d', strtotime('tomorrow')); ?>" value="<?= set_value('date') ?>" required/>
            </div>
            <div class="row form-group col-md-12">
                <label class="control-label col-md-4" for="hour">เวลา </label>
                <input type="text" class="form-control col-md-3 number_textbox" name="hour" maxlength="2" placeholder="ชั่วโมง" value="<?= set_value('hour') ?>" required/><p style="padding: 6px 12px; margin-bottom: 0px;">:</p>
                <input type="text" class="form-control col-md-3 number_textbox" name="minute" maxlength="2" placeholder="นาที" value="<?= set_value('minute') ?>" required/><p style="padding: 6px 12px; margin-bottom: 0px;">น.</p>
            </div>
            <div class="row form-group col-md-12" >
                <label class="control-label col-md-4" for="description">รายละเอียดการนัด </label>
                <textarea class="form-control col-md-8" rows="3" name="description" placeholder="รายละเอียดการนัด" value="<?= set_value('description') ?>" required></textarea>
            </div>
            <div class="row form-group col-md-12" style="margin-bottom: 0px">
                <button type="submit" class="btn btn-success mx-1">บันทึก</button>
            </div>
        </form>
    </div>
    
    <br>
    <?php if (session()->get('success-medhistory')): ?>
    <div class="alert alert-success" role="alert">
        <?= session()->get('success-medhistory'); ?>
    </div>
    <?php elseif (session()->get('danger-valid')): ?>
    <div class="col-12">
        <div class="alert alert-danger" role="alert">
            <?= session()->get('danger-valid'); ?>
        </div>
    </div>
    <?php endif; ?>
    <div class="container border border-secondary p-4 collapse" id="create-med">
        <h5>เพิ่มบันทึกการรักษา</h5>
        <hr>
        <form action="/create_medhistory" method="post" class="needs-validation" enctype="multipart/form-data" novalidate>
            <div class="row form-group col-md-12">
                <input type="hidden" name="age" value="<?= $age; ?>"/>
                <input type="hidden" name="patient_id" value="<?= $patient['id']; ?>"/>
                <label class="control-label col-md-1" for="weight">น้ำหนัก </label>
                <input type="text" class="form-control col-md-2 number_decimal_textbox" name="weight" placeholder="น้ำหนัก" value="<?= set_value('weight') ?>" required/>
                <p class="col-md-1"></p>
                <label class="control-label col-md-1" for="height">ส่วนสูง </label>
                <input type="text" class="form-control col-md-2 number_decimal_textbox" name="height" placeholder="ส่วนสูง" value="<?= set_value('height') ?>" required/>
            </div>
            <div class="row form-group col-md-12">
                <label class="control-label col-md-2" for="pressuresys">ความดัน SYS/mmhg </label>
                <input type="text" class="form-control col-md-1 number_textbox" name="pressuresys" placeholder="SYS/mmhg" value="<?= set_value('pressuresys') ?>"/>
                <label class="control-label col-md-2" style="text-align: right" for="pressuredia">DIA/mmhg </label>
                <input type="text" class="form-control col-md-1 number_textbox" name="pressuredia" placeholder="DIA/mmhg" value="<?= set_value('pressuredia') ?>"/>
                <p class="col-md-1"></p>
                <label class="control-label col-md-1" for="pulse">ชีพจร </label>
                <input type="text" class="form-control col-md-2 number_textbox" name="pulse" placeholder="ชีพจร" value="<?= set_value('pulse') ?>"/>
            </div>
            <div class="row form-group col-md-12">
                <label class="control-label col-md-1" for="temperature">อุณหภูมิ </label>
                <input type="text" class="form-control col-md-2 number_decimal_textbox" name="temperature" placeholder="อุณหภูมิ" value="<?= set_value('temperature') ?>"/>
                <p class="col-md-1"></p>
                <label class="control-label col-md-1" for="bmi">BMI </label>
                <input type="text" class="form-control col-md-2 number_decimal_textbox" name="bmi" placeholder="BMI" value="<?= set_value('bmi') ?>"/>
            </div>
            <div class="row form-group col-md-12">
                <label class="control-label col-md-12" for="description">คำอธิบายการรักษา </label>
                <textarea class="form-control col-md-8" rows="10" name="description" placeholder="คำอธิบายการรักษา" value="<?= set_value('description') ?>" required></textarea>
            </div>
            <div class="row form-group col-md-12" id="create-med-preview">
                <label class="control-label col-md-12" for="image">รูปภาพประกอบการรักษา<span style="color:#999"> (สูงสุด 20 รูป)</span></label>
                <input type="file" class="form-control" name="image[]" id="image-btn" multiple/>
            </div>
            <div class="row form-group col-md-12" style="margin-bottom: 0px">
                <button type="submit" class="btn btn-success mx-1">บันทึก</button>
            </div>
        </form>
    </div>
    <br>
    <br>
    
    <h4>ประวัติการรักษา</h4>
    <div class="row form-group col-md-12">
        <div class="col-md-6"></div>
        <label class="control-label" style="margin: 0px 5px;">ค้นหา</label>
        <input type="date" id="from_date" max="<?= date('Y-m-d'); ?>"/>
        <label class="control-label" style="margin: 0px 5px;">ถึง</label>
        <input type="date" id="to_date" max="<?= date('Y-m-d'); ?>"/>
        <button type="button" class="btn btn-sm btn-info mx-1" id="search_med">ค้นหา</button>
        <button type="button" class="btn btn-sm btn-danger" id="serch_reset">ยกเลิก</button>
    </div>
    <div id="medhistory">
        <div>
            <div id="medhistory_info">
                <?php if (!empty($medhistory) && is_array($medhistory)) :
                    foreach ($medhistory as $keys=>$medhistory_item):?>
                        <div>
                            <div class="container border border-secondary p-3">
                                <div class="row form-group col-md-12">
                                    <div class="col-md-4"><b>วันที่บันทึก :</b> <?=$medhistory_item['date']; ?></div>
                                    <div class="col-md-4"><b>บันทึกโดย :</b> <?= $medhistory_item['name'] . ' ' .$medhistory_item['lastname']; ?></div>
                                </div>
                                <div class="row form-group col-md-12">
                                    <div class="col-md-4"><b>อายุ :</b> <?= $medhistory_item['age']; ?> ปี</div>
                                    <div class="col-md-2"><b>น้ำหนัก :</b> <?= $medhistory_item['weight']; ?> กิโลกรัม</div>
                                    <div class="col-md-3"><b>ส่วนสูง :</b> <?= $medhistory_item['height']; ?> เซนติเมตร</div>
                                </div>
                                <div class="row form-group col-md-12">
                                    <?php if(!empty($medhistory_item['pressuresys']) && !empty($medhistory_item['pressuredia'])): ?>
                                    <div class="col-md-4"><b>ความดัน :</b> SYS/mmhg <?= $medhistory_item['pressuresys'].' &nbsp;&nbsp;DIA/mmhg '.$medhistory_item['pressuredia']; ?></div>
                                    <?php else: ?>
                                    <div class="col-md-4"><b>ความดัน :</b> ไม่มีข้อมูล</div>
                                    <?php endif; 
                                    if(!empty($medhistory_item['pulse'])): ?>
                                    <div class="col-md-2"><b>ชีพจร :</b> <?= $medhistory_item['pulse']; ?> PUL/min</div>
                                    <?php else: ?>
                                    <div class="col-md-4"><b>ชีพจร :</b> ไม่มีข้อมูล</div>
                                    <?php endif; ?>
                                </div>
                                <div class="row form-group col-md-12">
                                    <?php if(!empty($medhistory_item['temperature'])): ?>
                                    <div class="col-md-4"><b>อุณหภูมิ :</b> <?= $medhistory_item['temperature']; ?> องศาเซลเซียส</div>
                                    <?php else: ?>
                                    <div class="col-md-4"><b>อุณหภูมิ :</b> ไม่มีข้อมูล</div>
                                    <?php endif; 
                                    if(!empty($medhistory_item['bmi'])): ?>
                                    <div class="col-md-2"><b>BMI :</b> <?= $medhistory_item['bmi']; ?></div>
                                    <?php else: ?>
                                    <div class="col-md-4"><b>BMI :</b> ไม่มีข้อมูล</div>
                                    <?php endif; ?>
                                </div>
                                <div class="row form-group col-md-12">
                                    <div class="col-md-12"><b>คำอธิบายการรักษา</b></div>
                                    <div class="col-md-10"><?= $medhistory_item['description']; ?></div>
                                </div>
                                <?php if(isset($medhistory_item['image']) && !empty($medhistory_item['image'])):
                                    $path = unserialize($medhistory_item['image']); ?>
                                <div class="row form-group col-md-12">
                                    <button class="btn btn-outline-primary" type="button" data-toggle="collapse" data-target="#image-<?= $keys; ?>" aria-expanded="false" aria-controls="image-<?= $keys; ?>">
                                        รูปภาพประกอบการรักษา
                                    </button>
                                    <div class="col-md-10 collapse" id="image-<?= $keys; ?>">
                                        <br>
                                        <?php foreach ($path as $path_item): ?>
                                        <img src="<?php echo base_url() . $path_item; ?>" onclick="image_modal(this)" style="max-width:100px;max-height:100px" id="myImg"/>
                                        <?php endforeach; ?>
                                    </div>
                                </div>
                                <?php endif; ?>

                            </div>
                            <br>
                        </div>
                    <?php endforeach;
                    else : ?>
                        <div class="container border border-secondary p-3">
                            <div class="text-center">ไม่มีประวัติการรักษา</div>
                        </div>
                        <br>
            <?php endif; ?>
            </div>
            <div id="med_search" hidden>
                
            </div>
        </div>
    </div>
    <a  class="btn btn-danger" href="/">ย้อนกลับ</a>
</div>

<div id="image-modal" class="modal">
    <span class="close" onclick="close_modal()">&times;</span>
    <img class="modal-content" id="img">
</div>