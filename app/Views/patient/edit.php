<div class="container border p-3">
    <h2><?= esc($title); ?></h2>
    <hr>
    <?php if (isset($validation)): ?>
        <div class="col-12">
            <div class="alert alert-danger" role="alert">
                <?= $validation->listErrors(); ?>
            </div>
        </div>
    <?php endif; ?>

    <form action="/edit/<?= esc($patient['id']); ?>" method="post" class="needs-validation" novalidate>
        <label class="control-label col-md-3" style="color: red">ห้ามเว้นว่างช่องที่มี *</label>
        <div class="form-group input-group">
            <label class="control-label col-md-2">Hospital Number (HN) </label>
            <input type="text" class="form-control col-md-4" name="hospitalnum" placeholder="Hospital Number (HN)" value="<?= $patient['hospitalnum']; ?>" required/>
            <label class="control-label " style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">ชื่อ </label>
            <input type="text" class="form-control col-md-4" name="name" placeholder="ชื่อ" value="<?= $patient['name']; ?>" required/>
            <label class="control-label " style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">นามสกุล </label>
            <input type="text" class="form-control col-md-4" name="lastname" placeholder="นามสกุล" value="<?= $patient['lastname']; ?>" required/>
            <label class="control-label " style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">เพศ </label>
            <select class="form-control col-md-1" name="sex" required/>
                <option value="0"<?php  if( $patient['sex'] === '0') echo 'selected'; ?>>ชาย</option>
                <option value ="1"<?php if( $patient['sex'] === '1') echo 'selected'; ?> >หญิง</option>
            </select>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">สถานภาพสมรส </label>
            <select class="form-control col-md-2" name="marriage" required>
                <option value="0"<?php  if( $patient['marriage'] === '0') echo 'selected'; ?> >โสด</option>
                <option value ="1"<?php if($patient['marriage'] === '1' ) echo 'selected'; ?> >สมรส</option>
                <option value ="2"<?php if($patient['marriage'] === '2' ) echo 'selected'; ?> >หม้าย</option>
                <option value ="3"<?php if($patient['marriage'] === '3' ) echo 'selected'; ?> >หย่า</option>
                <option value ="4"<?php if($patient['marriage'] === '4' ) echo 'selected'; ?> >แยกกันอยู่</option>
            </select>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">กรุ๊ปเลือด </label>
            <select class="form-control col-md-1" name="blood" required>
                <option value="0"<?php if( $patient['blood'] === '0') echo 'selected'; ?>>O</option>
                <option value ="1"<?php if( $patient['blood'] === '1') echo 'selected'; ?>>A</option>
                <option value ="2"<?php if( $patient['blood'] === '2') echo 'selected'; ?>>B</option>
                <option value ="3"<?php if( $patient['blood'] === '3') echo 'selected'; ?>>ABO</option>
            </select>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>

            <label class="control-label col-md-1">Rh </label>
            <select class="form-control col-md-1" name="rh" required>
                <option value="0" <?php if( $patient['rh'] === '0') echo 'selected'; ?>>+</option>
                <option value ="1"<?php if( $patient['rh'] === '1') echo 'selected'; ?>>-</option>
            </select>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">ประวัติการแพ้ยา :</label>
            <input type="text" class="form-control col-md-4" name="allergic" placeholder="ประวัติการแพ้ยา" value="<?= $patient['allergic']; ?>" />
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">วัน/เดือน/ปี เกิด </label>
            <input type="date" name="bdate" value="<?= $patient['bdate']; ?>" max="<?= date("Y-m-d"); ?>" required>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2 number_textbox">เลขประจำตัวประชาชน </label>
            <input type="text" class="form-control col-md-2" name="citizennum" placeholder="เลขประจำตัวประชาชน" value="<?= $patient['citizennum']; ?>" minlength="13" maxlength="13">
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">เบอร์โทรศัพท์ </label>
            <input type="text" class="form-control col-md-2 number_textbox" name="phone" placeholder="เบอร์โทรศัพท์" value="<?= $patient['phone']; ?>">
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">ที่อยู่ </label>
            <textarea class="form-control col-md-4" name="address" required><?= $patient['address']; ?></textarea>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">จังหวัด </label>
            <select class="form-control col-md-4" name="province" id="province" required>
                <option>กรุณาเลือกจังหวัด</option>
                <?php if (!empty($province) && is_array($province)) :
                foreach ($province as $province_item): ?>
                <option value="<?= $province_item['id']; ?>" ><?= $province_item['name_th']; ?></option>
                <?php endforeach; endif; ?>
            </select>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>
            <input type="hidden" id='provincevalue' value="<?= $patient['province']; ?>" required/>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">เขต/อำเภอ </label>
            <select class="form-control col-md-4" name="district" id="district" required>
                <option value='<?= $patient['district']; ?>' selected>กรุณาเลือกเขต/อำเภอ</option>
            </select>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">แขวง/ตำบล </label>
            <select class="form-control col-md-4" name="subdistrict" id="subdistrict" required>
                <option value='<?= $patient['subdistrict']; ?>' selected>กรุณาเลือกแขวง/ตำบล</option>
            </select>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>
        </div>
        <div class="form-group input-group">
            <label class="control-label col-md-2">รหัสไปรษณีย์ </label>
            <input type="text" class="form-control col-md-4 number_textbox" name="zipcode" value="<?= $patient['zipcode']; ?>" required>
            <label class="control-label" style="color: red; margin-left: 5px">* </label>
        </div>

        <div class="form-group input-group">
            <button type="submit" class="btn btn-success mx-1">บันทึก</button>
            <a  class="btn btn-danger" href="/">ย้อนกลับ</a>
        </div>
    </form>
</div>
