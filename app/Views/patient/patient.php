<div class="container border p-3">
    <h2><?= esc($title); ?></h2>
    <hr>
<?php if (session()->get('success')): ?>
    <div class="alert alert-success" role="alert">
        <?= session()->get('success'); ?>
    </div>
<?php elseif (session()->get('no-data')): ?>
    <div class="alert alert-danger" role="alert">
        <?= session()->get('no-data'); ?>
    </div>
<?php endif; ?>

    <a class="btn btn-success float-md-left" href="/create">เพิ่มข้อมูลผู้ป่วย</a>
    
    <table id="allpatient" class="table table-striped table-hover" style="width: 100%">
        <thead>
            <tr>
                <th class="text-left" >Hospital Number (HN)</th>
                <th class="text-left" >ชื่อ-นามสกุล</th>
                <th class="text-left" >รหัสประจำตัวประชาชน</th>
                <th class="text-left" >เบอร์โทรศัพท์</th>
                <th class="text-right" ></th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($patient) && is_array($patient)) :
                foreach ($patient as $patient_item): ?>
            <tr>
                <td class="text-left"><?= $patient_item['hospitalnum']; ?></td>
                <td class="text-left"><?= $patient_item['name'] .' '. $patient_item['lastname']; ?></td>
                <td class="text-left"><?php if(!empty($patient_item['citizennum'])):
                                                        echo $patient_item['citizennum']; 
                                                    else :
                                                        echo 'ไม่มีข้อมูล' ;
                                                    endif; ?></td>
                <td class="text-left"><?php if(!empty($patient_item['phone'])):
                                                        echo $patient_item['phone'];; 
                                                    else :
                                                        echo 'ไม่มีข้อมูล' ;
                                                    endif; ?></td>
                <td class="text-right">
                    <a class="btn btn-sm btn-primary mx-1" href="/view/<?= $patient_item['id'] ?>">ดูข้อมูล</a>
                    <a class="btn btn-sm btn-info mx-1" href="/edit/<?= $patient_item['id']; ?>">แก้ไข</a>
                    <button type="button" class="btn btn-sm btn-danger mx-1" data-href="<?= $patient_item['id']; ?>" data-toggle="modal" data-target="#confirm-delete">ลบ</button>
                </td>
            </tr>
            <?php endforeach; 
            else : ?>
            <tr>
                <td class="text-center">ไม่มีข้อมูลผู้ป่วย</td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
                <td class="text-center"></td>
            </tr>
            <?php endif; ?>
        </tbody>
    </table>
    
    <div class="modal fade" id="confirm-delete" tabindex="-1" role="dialog" aria-labelledby="mymodalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    ยืนยันการลบ
                </div>
                <div class="modal-body">
                    ต้องการลบใช่หรือไม่ ?
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">ยกเลิก</button>
                    <a class="btn btn-danger btn-ok patientClickDelete">ยืนยัน</a>
                </div>
            </div>
        </div>
    </div>
</div>