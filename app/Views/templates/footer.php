
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<script src="https://unpkg.com/bootstrap@4.5.0/dist/js/bootstrap.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"></script>
<script src="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.21/sorting/date-euro.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
 <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
<script type="text/javascript" src="<?php echo base_url('/js/address.js'); ?>"></script>
<script type="text/javascript" src="<?php echo base_url('/js/jquery.e-calendar.js'); ?>"></script>

<script type="text/javascript">
$(document).ready(function () {
    
    //data table patient
    $('#allpatient').DataTable({
        "lengthChange": false,
        "columnDefs": [
            { "searchable": false, "targets": [0, 3, 4] }
          ]
    });
    $('#allpatient_filter').addClass("float-md-right");
    
    //data table appointment
    $('#allappointment').DataTable({
        "lengthChange": false,
        "order": [[0, "desc"]],
        "columnDefs": [
            { "searchable": false, "targets": [2, 3] },
            { "type": "date-euro", "targets": 0 }
          ]
    });
    $('#allappointment_filter').addClass("float-md-right");

    //delete modal
   $('#confirm-delete').on('show.bs.modal', function (e) {
        $(this).find('.btn-ok').attr('href', $(e.relatedTarget).data('href'));
    });
    
    $('.patientClickDelete').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: "<?= base_url() . "/delete/"?>" + $(this).attr('href'),
            data: '',
            success: function (obj) {
                location.reload();
            }
        });
    });
    
    $('.appointmentClickDelete').on('click', function (e) {
        e.preventDefault();
        $.ajax({
            type: "GET",
            url: "<?= base_url() . "/appointment/delete/"?>" + $(this).attr('href'),
            data: '',
            success: function (obj) {
                location.reload();
            }
        });
    });
    
    //appointment edit
    <?php $uri = service('uri');
    if($uri->getSegment(1) == 'appointment'): ?>
        $('.btn-info').on('click', function(){
            var id = $(this).data('id');
            var index = $(this).data('index');
            var appointment = <?php echo json_encode($appointment)?>;
            var datetime = appointment[index]['date'].split(' ');
            time = datetime[1].split(':');
            
            document.getElementById("appointmentEdit").removeAttribute('hidden');
            document.getElementById("editTitle").innerHTML = "แก้ไขการนัดหมาย | " + appointment[index]['name'] + " " + appointment[index]['lastname'];
            
            document.getElementsByName("id")[0].value = id;
            document.getElementsByName("date")[0].value = datetime[0];
            document.getElementsByName("hour")[0].value = time[0];
            document.getElementsByName("minute")[0].value = time[1];
            document.getElementsByName("description")[0].value = appointment[index]['description'].replace(/<\/br>/g, "\n");
        });
        
        $('#cancelEdit').on('click', function(){
            document.getElementById("appointmentEdit").setAttribute('hidden', true);
        });
    <?php endif; ?>
    
    //search medical history
    $('#search_med').on('click', function(){
        var from_date = document.getElementById("from_date").value;
        var to_date = document.getElementById("to_date").value;
        <?php $uri = service('uri'); ?>
        <?=($uri->getSegment(1) == 'view' ? 'var id = '. $patient['id'].';':'') ?>
        $.ajax({
             type: "POST",
             url: location.origin+"/search_medhistory",
             data: {id : id,from_date : from_date, to_date : to_date},
             dataType: "html",
             success: function(html){
                 console.log(from_date+' '+to_date);
                 console.log(html);
                 document.getElementById("medhistory_info").setAttribute('hidden', true);
                 $("#med_search").html(html);
                 document.getElementById("med_search").removeAttribute('hidden');
             }
         });
    });
    
    $('#serch_reset').on('click', function(){
        document.getElementById("medhistory_info").removeAttribute('hidden');
        document.getElementById("med_search").setAttribute('hidden', true);
        document.getElementById("from_date").value = '';
        document.getElementById("to_date").value = '';
        document.getElementById("from_date").setAttribute('max', '<?= date('Y-m-d'); ?>');
        document.getElementById("to_date").removeAttribute('min');
    });
    
    $("#from_date").change(function(){
        document.getElementById("to_date").setAttribute('min', document.getElementById("from_date").value);
    });
    
    $("#to_date").change(function(){
        document.getElementById("from_date").setAttribute('max', document.getElementById("to_date").value);
    });
    
});

$('.number_decimal_textbox').on('input', function(event){
    let new_text = event.target.value;
    let lastValid = new_text.substring(0, new_text.length-1)
    let validNumber = new RegExp(/^\d*\.?\d*$/);
   
    if (validNumber.test(new_text)) {
        lastValid = event.value;
      } else {
        event.target.value = lastValid;
      }
    });
    
$('.number_textbox').on('input', function(event){
    let new_text = event.target.value;
    let lastValid = new_text.substring(0, new_text.length-1)
    let validNumber = new RegExp(/^\d*$/);
   
    if (validNumber.test(new_text)) {
        lastValid = event.value;
      } else {
        event.target.value = lastValid;
      }
    });
</script>
<script>
// Disable form submissions if there are invalid fields
(function() {
  'use strict';
  window.addEventListener('load', function() {
    // Get the forms we want to add validation styles to
    var forms = document.getElementsByClassName('needs-validation');
    // Loop over them and prevent submission
    var validation = Array.prototype.filter.call(forms, function(form) {
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    });
  }, false);
})();
</script>
<script>
    //Appointment Calendar
    $(document).ready(function () {
        $('#calendar').eCalendar({
            weekDays: ['Sun','Mon','Tue','Wed','Thu','Fri','Sat'],
            months: ['January','February','March','April','May','June','July','August','September','October','November','December'],
            textArrows: {previous:'<', next:'>'},
            eventTitle:'Events',
            url:'',
            firstDayOfWeek: 0,
            events: [
            <?php if (!empty($appointment) && is_array($appointment)) :
                foreach ($appointment as $appointment_item) : ?>
                  {title:'<?= $appointment_item['name']. ' ' .$appointment_item['lastname']; ?>', description:'<?= $appointment_item['description']; ?>', datetime:new Date(('<?= $appointment_item['date']; ?>').replace(' ', 'T')+'.000+07:00'), url: "<?= base_url('/view/' .$appointment_item['patient_id']); ?> ", url_blank: true},
            <?php endforeach; endif; ?>
            ]
        });
    });
</script>
<script>
    //image modal
    $(document).click(function (e) {
        if ($(e.target).is('#image-modal')) {
            $('#image-modal').fadeOut(500);
        }
    });
    
    var modal = document.getElementById("image-modal");
    var modalImg = document.getElementById("img");
    function image_modal(evt) {
        modal.style.display = "block";
        modalImg.src = evt.src;
    }
    
    // When the user clicks on <span> (x), close the modal
    function close_modal() {
        $('#image-modal').fadeOut(500);
    };
    
    //create medical history preview image
    function readURL(input) {
            if (input.files && input.files[0]) {
                for (var i = 0; i<input.files.length; i++){
                    var reader = new FileReader();
                    reader.onload = function (e){
                        $("#create-med-preview").append("<img src=\""+e.target.result+"\" class=\"img-preview\" style=\"max-width:100px; max-height:100px; padding: 3px\" />")
                    };
                    reader.readAsDataURL(input.files[i]);
                }
            }
        }

        $("#image-btn").change(function () {
        if (this.files.length > 20){
            alert("ไม่สามารถอัปโหลดรูปเกิน 20 รูปได้");
            this.value = "";
            $(".img-preview").remove();
        }
            readURL(this);
        });
</script>
</body>
</html>