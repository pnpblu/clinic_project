<!DOCTYPE html>
<!--
To change this license header, choose License Headers in Project Properties.
To change this template file, choose Tools | Templates
and open the template in the editor.
-->
<html lang="th">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
        <title><?=$title?></title>
        <link rel="stylesheet" href="https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css" >
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="<?php echo base_url('/css/jquery.e-calendar.css'); ?>" >
        <?php $uri = service('uri'); ?>
        <?=($uri->getSegment(1) == 'profile' ? 
        '<link rel="stylesheet" href="'.base_url('/css/style.css'). '" >'
        : '') ?>
        <?php $uri = service('uri'); ?>
        <?=($uri->getSegment(1) == 'view' ? 
        '<link rel="stylesheet" href="'. base_url('/css/image-modal.css').'" >'
        : '') ?>
    </head>
    <body>