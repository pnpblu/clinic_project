<?php $uri = service('uri'); ?>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <div class="container">
        <div class="navbar-header d-block d-lg-none">
                <a type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#navbarSupportedContent" aria-expanded="false" aria-controls="navbar">
                    <i class="fa fa-bars" style="color:white; font-size:24px"></i>
                    <span class="sr-only">Toggle navigation</span>
                </a>
        </div>
            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item <?=($uri->getSegment(1) == '' ? 'active' : null) ?>">
                        <a class="nav-link" href="/">รายชื่อผู้ป่วย </a>
                    </li>
                    <li class="nav-item <?=($uri->getSegment(1) == 'appointment' ? 'active' : null) ?>">
                        <a class="nav-link" href="/appointment">ตารางนัดผู้ป่วย</a>
                    </li>
                    <?php if (session()->get('permission') === '0') : ?>
                    <li class="nav-item <?=($uri->getSegment(1) == 'approve' ? 'active' : null) ?>">
                        <a class="nav-link" href="/approve">อนุมัติบัญชีผู้ใช้</a>
                    </li>
                    <li class="nav-item <?=($uri->getSegment(1) == 'manage' ? 'active' : null) ?>">
                        <a class="nav-link" href="/manage">จัดการบัญชีผู้ใช้</a>
                    </li>
                    <?php endif; ?>
                </ul>
                <ul class="navbar-nav my-2 my-lg-0">
                    <li class="nav-item <?=($uri->getSegment(1) == 'profile' ? 'active' : null) ?>">
                        <a class="nav-link" href="/profile"><?=session()->get('name'). ' ' .session()->get('lastname') ?></a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="/logout">ออกจากระบบ</a>
                    </li>
                </ul>
            </div>
    </div>
</nav>