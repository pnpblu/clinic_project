<div class="container border p-3">
    <h2><?= esc($title); ?></h2>

<?php if (session()->get('approve-success')): ?>
    <div class="alert alert-success" role="alert">
        <?= session()->get('approve-success'); ?>
    </div>
<?php elseif (session()->get('approve-danger')): ?>
    <div class="alert alert-danger" role="alert">
        <?= session()->get('approve-danger'); ?>
    </div>
<?php endif; ?>
    
    <table id="allapprove" class="table table-striped table-hover" style="width: 100%">
        <thead>
            <tr>
                <th class="text-left" >ชื่อ-นามสกุล</th>
                <th class="text-right" ></th>
            </tr>
        </thead>
        <tbody>
            <?php if (!empty($new_user) && is_array($new_user)) :
                foreach ($new_user as $new_user_item) : ?>
            <tr>
                <td class="text-left"><?= $new_user_item['name'] .' '. $new_user_item['lastname']; ?></td>
                <td class="text-right">
                    <a class="btn btn-sm btn-primary mx-1" href="/approve/0/<?= $new_user_item['id'] ?>">อนุมัติ</a>
                    <a class="btn btn-sm btn-info mx-1" href="/deleteUser/<?= $new_user_item['id'] ?>">ปฏิเสธ</a>
                </td>
            </tr>
            <?php endforeach; 
            else : ?>
            <tr>
                <td class="text-center">ไม่มีผู้ใช้งานใหม่</td>
                <td></td>
            </tr>
            <?php endif; ?>
        </tbody>
    </table>
    <br>
    <div class="row">
        <div class="form-group col-12 col-sm-12">
            <a  class="btn btn-danger" href="/">ย้อนกลับ</a>
        </div>
    </div>
</div>