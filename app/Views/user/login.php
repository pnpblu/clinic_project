<html lang="th">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"> 
        <title><?=$title?></title>
        <link rel="stylesheet" href="https://unpkg.com/bootstrap@4.5.0/dist/css/bootstrap.min.css" >
        <link rel="stylesheet" href="<?php echo base_url('/css/style.css'); ?>" >
    </head>
    
    <body>
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-8 offset-sm2 col-md-6 offset-md-3 mt-5 pt-3 pb-3 bg-white form-wrapper">
                    <div class="container">
                        <h3>กาญจนาภิเษก-บางบอนคลินิก</h3>
                        <hr>
                        <?php if (session()->get('success')): ?>
                        <div class="alert alert-success" role="alert">
                            <?= session()->get('success'); ?>
                        </div>
                        <?php elseif (session()->get('danger')): ?>
                        <div class="alert alert-danger" role="alert">
                            <?= session()->get('danger'); ?>
                        </div>
                        <?php endif; ?>
                        <form class="" action="/login" method="post" id="formLogin">
                            <div class="form-group">
                                <label for="username">ชื่อผู้ใช้</label>
                                <input  type="text" class="form-control" placeholder="ชื่อผู้ใช้งาน" name="username" value="<?= set_value('username') ?>" required>
                            </div>
                            <div class="form-group">
                                <label for="password">รหัสผ่าน</label>
                                <input  type="password" class="form-control" placeholder="รหัสผ่าน" name="password" required>
                            </div>
                            <?php if (isset($validation)): ?>
                                    <div class="col-12">
                                        <div class="alert alert-danger" role="alert">
                                            <?= $validation->listErrors(); ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            <div class="row">
                                <div class="col-12 col-sm-4">
                                    <button type="submit" class="btn btn-primary">เข้าสู่ระบบ</button>
                                </div>
                                <div class="col-12 col-sm-8 text-right">
                                    <a href="/register">สร้างบัญชีผู้ใช้</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://unpkg.com/bootstrap@4.5.0/dist/js/bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"></script>
        <script src="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
     </body>
</html>