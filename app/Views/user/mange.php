<div class="container border p-3">
    <h2><?= esc($title); ?></h2>
    
    <?php if (session()->get('manage-success')): ?>
    <div class="alert alert-success" role="alert">
        <?= session()->get('manage-success'); ?>
    </div>
    <?php endif; ?>
    <hr>
    <div class="row">
        <div class="col-12 col-sm-6" style="text-align: center">
            <h6 style="font-weight: bold;">ชื่อ-นามสกุล</h6>
        </div>
        <div class="col-12 col-sm-6" style="text-align: center">
            <h6 style="font-weight: bold;">สถานะ</h6>
        </div>
    </div>
    <form action="/manage" method="post">
        <?php if (!empty($user) && is_array($user)) :
                    foreach ($user as $user_item) : ?>
        
        <div class="row border">
            <div class="col-12 col-sm-6" style="text-align: center">
                <div class="form-group" style="margin-bottom: 0px; padding: 10px">
                    <label style="margin-bottom: 0px"><?= $user_item['name'] . ' ' . $user_item['lastname'] ?> </label>
                </div>
            </div>
            <div class="col-12 col-sm-6" style="text-align: center">
                <div class="form-group" style="margin-bottom: 0px; padding: 6px">
                    <input type="radio" style="margin-bottom: 0px" name="status_<?= $user_item['id'] ?>" value="0" <?= ($user_item['status'] == '0' ? 'checked' : null); ?>>
                    <label for="permission" style="padding-right: 15px">เปิดใช้งาน</label>
                    <input type="radio" style="margin-bottom: 0px"  name="status_<?= $user_item['id'] ?>" value="1" <?= ($user_item['status'] == '1' ? 'checked' : null); ?>>
                    <label for="permission" >ปิดใช้งาน</label>
                </div>
            </div>
        </div>
        <?php endforeach;
        else: ?>
        <div class="row border">
            <div class="col-12 col-sm-12" style="text-align: center">
                <div class="form-group" style="margin-bottom: 0px; padding: 10px">
                    <label style="margin-bottom: 0px">ไม่มีบัญชีผู้ใช้</label>
                </div>
            </div>
        </div>
        <?php endif; ?>
        <br>
        <div class="row">
            <div class="form-group col-12 col-sm-12">
                <?php if (!empty($user) && is_array($user)) : ?>
                <button type="submit" class="btn btn-success">บันทึก</button>
                <?php endif; ?>
                <a  class="btn btn-danger" href="/">ย้อนกลับ</a>
            </div>
        </div>
    </form>
</div>