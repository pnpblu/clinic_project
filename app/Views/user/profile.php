     
        <div class="container">
            <div class="row">
                <div class="col-12 col-sm-8 offset-sm2 col-md-6 offset-md-3 mt-5 pt-3 pb-3 bg-white form-wrapper">
                    <div class="container">
                        <h3><?= $user['name'] . ' ' . $user['lastname'] ?> | แก้ไขข้อมูลส่วนตัว</h3>
                        <hr>
                        <?php if (session()->get('profile-success')): ?>
                        <div class="alert alert-success" role="alert">
                            <?= session()->get('profile-success'); ?>
                        </div>
                        <?php endif; ?>
                        <form class="" action="/profile" method="post" id="formLogin">
                            <div class="row">
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="name">ชื่อ</label>
                                        <input  type="text" class="form-control" placeholder="ชื่อ" name="name" id="name" value="<?= set_value('name', $user['name']) ?>" required>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="name">นามสกุล</label>
                                        <input  type="text" class="form-control" placeholder="นามสกุล" name="lastname" id="lastname" value="<?= set_value('lastname', $user['lastname']) ?>" required>
                                    </div>
                                </div>
                                <div class="col-12">
                                    <div class="form-group">
                                        <label for="username">ชื่อผู้ใช้งาน</label>
                                        <input  type="text" class="form-control username_textbox" placeholder="ชื่อผู้ใช้งาน" name="username" id="username" value="<?= set_value('username', $user['username']) ?>" minlength="4" maxlength="20" required>
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="password">รหัสผ่าน</label>
                                        <input  type="password" class="form-control username_textbox" placeholder="รหัสผ่าน" name="password" id="password" minlength="8" maxlength="20">
                                    </div>
                                </div>
                                <div class="col-12 col-sm-6">
                                    <div class="form-group">
                                        <label for="password_confirm">ยืนยันรหัสผ่าน</label>
                                        <input  type="password" class="form-control username_textbox" placeholder="ยืนยันรหัสผ่าน" name="password_confirm" id="password_confirm" minlength="8">
                                    </div>
                                </div>
                                <?php if (isset($validation)): ?>
                                    <div class="col-12">
                                        <div class="alert alert-danger" role="alert">
                                            <?= $validation->listErrors(); ?>
                                        </div>
                                    </div>
                                <?php endif; ?>
                            </div>
                            
                            <div class="row">
                                <div class="form-group col-12 col-sm-12">
                                        <button type="submit" class="btn btn-primary">บันทึก</button>
                                        <a  class="btn btn-danger" href="/patient">ย้อนกลับ</a>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

        <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
        <script src="https://unpkg.com/bootstrap@4.5.0/dist/js/bootstrap.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
        <script src="https://cdn.datatables.net/1.10.21/js/dataTables.bootstrap4.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css"></script>
        <script src="https://cdn.datatables.net/1.10.21/css/dataTables.bootstrap4.min.css"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
         <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.0/js/bootstrap.min.js"></script>
     </body>
     <script>
        $('.username_textbox').on('input', function(event){
            let new_text = event.target.value;
            let lastValid = new_text.substring(0, new_text.length-1)
            let validNumber = new RegExp(/^[a-zA-Z0-9._-]*$/);

            if (validNumber.test(new_text)) {
                lastValid = event.value;
              } else {
                event.target.value = lastValid;
              }
        });
    </script>
</html>