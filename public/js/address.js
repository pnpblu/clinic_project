$(function(){
    var provinceObject = $('#province');
    var districtObject = $('#district');
    var subdistrictObject = $('#subdistrict');
 
     function setDistrict(data){
        var provinceId = data;
        districtObject.html('<option value="">กรุณาเลือกเขต/อำเภอ</option>');
        subdistrictObject.html('<option value="">กรุณาเลือกแขวง/ตำบล</option>');

         $.ajax({
             type: "GET",
             url: location.origin+"/getDistrict/"+provinceId,
             data: "",
             dataType: "json",
             success: function(data){
                 $.each(data, function(index, item){
                        districtObject.append(
                            $('<option></option>').val(item.id).html(item.name_th)
                    );
                 });
             }
         });
     }

    function setSubdistrict(data){
        var districtId = data;
        subdistrictObject.html('<option value="">กรุณาเลือกแขวง/ตำบล</option>');

        $.ajax({
            type: "GET",
            url: location.origin+"/getSubdistrict/"+districtId,
            data: "",
            dataType: "json",
            success: function(data){
                $.each(data, function(index, item){
                    subdistrictObject.append(
                        $('<option></option>').val(item.id).html(item.name_th)
                    );
                });
            }
       });
    }
    
    provinceObject.on('change', function(){
         setDistrict($(this).val());
     });
     districtObject.on('change', function(){
         setSubdistrict($(this).val());
     });
    
    if (window.location.pathname.includes('/edit/')){
        var province_data = $('#provincevalue').val();
        var district_data = districtObject.val();
        var subdistrict_data = subdistrictObject.val();
        setDistrict(province_data);
        setSubdistrict(district_data);
        
        document.getElementById('province').value = province_data;
        setTimeout(function(){
            document.getElementById('district').value = district_data;
            document.getElementById('subdistrict').value = subdistrict_data;
        }, 400);
        
    }
    
});